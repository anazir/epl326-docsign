#include <stdio.h>
#include <string.h>
#include <openssl/evp.h>
#include <openssl/rsa.h>
#include <openssl/pem.h>

char* read_file(char* path, int *len) {
  char *buffer = 0;
  FILE *f = fopen(path, "rb");

  if (f) {
    fseek(f, 0, SEEK_END);
    *len = ftell(f);
    fseek(f, 0, SEEK_SET);
    buffer = malloc(*len * sizeof(char));
    if (buffer) {
      fread(buffer, 1, *len, f);
    }
    fclose(f);
  }

  if (!buffer) {
    printf("Problem reading file\n");
    exit(1);
  }

  return buffer;
}

void validate_doc(char* path) {
  int buf_len;
  char* buffer = read_file(path, &buf_len);

  // Read keys
  char* public_filename = "public.pem";
  BIO *bp_read_public = BIO_new_file(public_filename, "r");
  RSA *keys = NULL;
  int ret;

  if (bp_read_public == NULL) {
    printf("Problem reading public key\n");
    exit(1);
  }

  // Read key
  keys = PEM_read_bio_RSAPublicKey(bp_read_public, NULL, NULL, NULL);
  if (keys == NULL) {
    printf("Problem reading keys\n");
    exit(1);
  }

  EVP_MD_CTX *ctx = EVP_MD_CTX_create();
  EVP_PKEY *pkey = EVP_PKEY_new();
  EVP_PKEY_assign_RSA(pkey, keys);
  size_t sig_len = 256;
  int doc_len = buf_len - sig_len;
  if (
    1 != EVP_DigestVerifyInit(ctx, NULL, EVP_sha256(), NULL, pkey) ||
    1 != EVP_DigestVerifyUpdate(ctx, buffer, doc_len)
  ) {
    printf("Problem verifying document.\n");
    exit(1);
  }

  ret = EVP_DigestVerifyFinal(ctx, (unsigned char*)(buffer + doc_len), sig_len);

  switch (ret) {
    case 1:
      printf("Digital signature is valid.\n");
      break;
    case 0:
      printf("Digital signature is not valid.\n");
      break;
    default:
      printf("Problem verifying document.\n");
      break;
  }
}

void sign_doc(char* path) {
  int buf_len;
  char* buffer = read_file(path, &buf_len);

  // Read keys
  char* private_filename = "private.pem";
  char* public_filename = "public.pem";
  BIO *bp_read_private = BIO_new_file(private_filename, "r");
  BIO *bp_read_public = BIO_new_file(public_filename, "r");
  RSA *keys = NULL;
  int ret;

  if (bp_read_private == NULL || bp_read_public == NULL) {
    // Generate keys
    keys = RSA_new();
    BIGNUM *bne = BN_new();
    ret = BN_set_word(bne, RSA_3);
    if (ret != 1) {
      printf("Problem generating keys\n");
      exit(1);
    }
    ret = RSA_generate_key_ex(keys, 2048, bne, NULL);
    if (ret != 1) {
      printf("Problem generating keys\n");
      exit(1);
    }

    BIO *bp_public = BIO_new_file(public_filename, "w");
    ret = PEM_write_bio_RSAPublicKey(bp_public, keys);
    if (ret != 1) {
      printf("Problem writing public key to file\n");
      exit(1);
    }

    BIO *bp_private = BIO_new_file(private_filename, "w");
    ret = PEM_write_bio_RSAPrivateKey(bp_private, keys, NULL, NULL, 0, NULL, NULL);
    if (ret != 1) {
      printf("Problem writing private key to file\n");
      exit(1);
    }
  } else {
    // Read keys
    keys = PEM_read_bio_RSAPrivateKey(bp_read_private, NULL, NULL, NULL);
    if (keys == NULL) {
      printf("Problem reading keys\n");
      exit(1);
    }
  }

  EVP_MD_CTX *ctx = EVP_MD_CTX_create();
  EVP_PKEY *pkey = EVP_PKEY_new();
  EVP_PKEY_assign_RSA(pkey, keys);
  unsigned char *sig = NULL;
  size_t sig_len = -1;
  if (
    !EVP_DigestSignInit(ctx, NULL, EVP_sha256(), NULL, pkey) ||
    !EVP_DigestSignUpdate(ctx, buffer, buf_len) ||
    !EVP_DigestSignFinal(ctx, sig, &sig_len)
  ) {
    printf("Problem getting signed digest of document\n");
    exit(1);
  }

  sig = calloc(sig_len, sizeof(unsigned char));
  if (!EVP_DigestSignFinal(ctx, sig, &sig_len)) {
    printf("Problem getting signed digest of document\n");
    exit(1);
  }

  // Get new filename
  char *file_name = strrchr(path, '/');
  if (file_name == NULL) {
    file_name = path;
  } else {
    file_name = file_name + 1;
  }
  char* new_path = calloc(strlen(file_name) + 7, sizeof(char));
  sprintf(new_path, "%s.signed", file_name);

  // Write to file
  FILE *fp = fopen(new_path, "w");
  fwrite(buffer, 1, buf_len, fp);
  fwrite(sig, 1, sig_len, fp);

  printf("Document %s was just signed. Signed document: %s\n", path, new_path);
}

int main (int argc, char *argv[]) {
  if (argc == 1) {
    printf("Usage: docsign -sv\n\n-s sign document\n-v validate signature\n\n");
  } else if (strcmp(argv[1], "-s") == 0) {
    if (argc < 3) {
      printf("No file specified\n");
      return 1;
    }

    sign_doc(argv[2]);
  } else if (strcmp(argv[1], "-v") == 0) {
    if (argc < 3) {
      printf("No file specified\n");
      return 1;
    }

    validate_doc(argv[2]);
  }

  return 0;
}
