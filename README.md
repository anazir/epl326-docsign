# EPL326, document signing and verification utility

To build run `make`.


To sign a document run:
```
./docsign -s <path-to-file>
```


To verify a document run
```
./docsign -v <path-to-file>
```
